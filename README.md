# Bug-Swiller

Service to swig down data from a certain bug/ticket tracking system that rhymes with nearer

# Auth-Jira Secret

Create a K8s secret containing the JIRA URL, a username and a password

```bash
kubectl -n quaff-dev create secret generic auth-jira --from-literal=jira.url=<YOUR_JIRA_URL> --from-literal=jira.username=<YOUR_JIRA_USERNAME> --from-literal=jira.password=<YOUR_JIRA_PASSWORD> 
```