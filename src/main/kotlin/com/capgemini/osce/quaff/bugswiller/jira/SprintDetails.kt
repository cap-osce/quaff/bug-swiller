package com.capgemini.osce.quaff.bugswiller.jira

import java.time.DayOfWeek
import java.time.LocalDate

class SprintDetails(val id: Int, val name: String, val startDate: LocalDate, val endDate: LocalDate) {

    var incompletePoints: Int = 0
    var completedPoints: Int = 0

    fun addCompletedPoints(points: Int): SprintDetails {
        completedPoints += points
        return this
    }

    fun addIncompletePoints(points: Int): SprintDetails {
        incompletePoints += points
        return this
    }

    fun getForecastToday(): Int {
        var workingDays = 0
        var passedDays = 0
        var date = startDate
        while (date.isBefore(endDate.plusDays(1))) {
            if (date.dayOfWeek != DayOfWeek.SATURDAY && date.dayOfWeek != DayOfWeek.SUNDAY) {
                ++workingDays
                if (date.isBefore(LocalDate.now())) {
                    ++passedDays
                }
            }
            date = date.plusDays(1)
        }


        return (completedPoints + incompletePoints) * passedDays / workingDays
    }

    fun getStatus(): String {
        val difference = getForecastToday() - completedPoints
        return if (difference > 8) "red" else if (difference > 0) "amber" else "green"
    }

}