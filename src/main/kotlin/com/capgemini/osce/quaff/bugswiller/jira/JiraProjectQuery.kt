package com.capgemini.osce.quaff.bugswiller.jira

import org.apache.commons.codec.binary.Base64
import org.springframework.beans.factory.annotation.Value
import org.springframework.http.MediaType
import org.springframework.stereotype.Component
import org.springframework.web.reactive.function.client.WebClient
import reactor.core.publisher.Flux
import java.nio.charset.Charset
import java.time.Instant
import java.time.LocalDate
import java.time.ZoneOffset

typealias JsonObject = Map<String,Any>
typealias JsonArray = List<JsonObject>

@Suppress("UNCHECKED_CAST")
@Component
class JiraProjectQuery(
        @Value("\${jira.server.username}") val username: String,
        @Value("\${jira.server.password}") val password: String,
        @Value("\${jira.server.url}") val jiraUrl: String
) {

    val webClient = WebClient.builder().baseUrl(jiraUrl)
            .defaultHeader("Authorization", "Basic " + Base64.encodeBase64String("${username}:${password}".toByteArray(Charset.defaultCharset())))
            .build()

    fun projects() =
            webClient.get().uri("/rest/api/latest/project")
                    .accept(MediaType.APPLICATION_JSON)
                    .retrieve()
                    .bodyToFlux(Map::class.java)
                    .map { mapOf(
                            "id" to it["key"],
                            "name" to it["name"]
                    )}

    fun boardId(key: String) =
            webClient.get().uri("/rest/greenhopper/latest/rapidviews/list?projectKey=" + key)
                    .accept(MediaType.APPLICATION_JSON)
                    .retrieve()
                    .bodyToFlux(Map::class.java)
                    .map { it["views"] as JsonArray }
                    .reduce { a, b -> a }
                    .flatMapMany { it -> Flux.fromIterable(it) }
                    .map { it["id"] as Int }

    fun sprints(boardId: Int) =
            webClient.get().uri("/rest/agile/1.0/board/${boardId}/sprint")
                    .retrieve()
                    .bodyToMono(Map::class.java)
                    .map { it["values"] as JsonArray }
                    .flatMapMany { it -> Flux.fromIterable(it) }
                    .filter { it["state"].toString() == "active" }
                    .map {
                        SprintDetails(
                            it["id"] as Int,
                            it["name"] as String,
                            parseDate(it["startDate"] as String),
                            parseDate(it["endDate"] as String))
                    }

    fun sprintStatus(sprint: SprintDetails) =
            webClient.get().uri("/rest/agile/1.0/sprint/${sprint.id}/issue")
                    .retrieve()
                    .bodyToMono(Map::class.java)
                    .map { it["issues"] as JsonArray }
                    .flatMapMany { it -> Flux.fromIterable(it) }
                    .map { it["fields"] as JsonObject }
                    .reduce(sprint) { details, ticket ->
                        val points = ticket["customfield_10105"] as Double?
                        if (statusDone(ticket["status"] as JsonObject))
                            details.addCompletedPoints(points?.toInt() ?: 0)
                        else
                            details.addIncompletePoints(points?.toInt() ?: 0)

                    }

    fun sprintStatusSummary(sprintDetails: SprintDetails) =
            mapOf(
                    "id" to sprintDetails.name,
                    "status" to sprintDetails.getStatus()
            )

    private fun parseDate(text: String): LocalDate {
        return LocalDate.ofInstant(Instant.parse(text), ZoneOffset.UTC)
    }

    private fun statusDone(status: JsonObject): Boolean {
        val category = status["statusCategory"] as JsonObject
        val key = category["key"] as String
        return key == "done"
    }
}