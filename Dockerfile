FROM openjdk:10-slim
COPY ./target/app.jar /app/app.jar
ENV ENV=dev
ENTRYPOINT ["/bin/sh", "-c"]
CMD ["java -Dspring.profiles.active=${ENV} -jar /app/app.jar"]
