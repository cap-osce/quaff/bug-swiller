package com.capgemini.osce.quaff.bugswiller

import com.capgemini.osce.quaff.bugswiller.jira.JiraProjectQuery
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RestController
import java.util.*

@RestController
class RestController(val jiraProjectQuery: JiraProjectQuery) {

    @GetMapping("/projects")
    fun namespaceStatus() =
            jiraProjectQuery.projects()
                    .sort { a, b -> a["id"].toString().compareTo(b["id"].toString()) }
                    .reduce(mapOf("projects" to LinkedList<Map<String,Any?>>())) {
                        projects, project -> projects["projects"]?.add(project)
                        projects
                    }

    @GetMapping("/projects/{key}")
    fun projectSprintStatus(@PathVariable key: String) =
            jiraProjectQuery.boardId(key)
                    .flatMap { jiraProjectQuery.sprints(it) }
                    .flatMap { jiraProjectQuery.sprintStatus(it) }
                    .map(jiraProjectQuery::sprintStatusSummary)
                    .sort { a, b -> a["id"]!!.compareTo(b["id"]!!)}
                    .reduce(mapOf("sprints" to LinkedList<Map<String,String>>())) {
                        sprints, sprint -> sprints["sprints"]?.add(sprint)
                        sprints
                    }

}