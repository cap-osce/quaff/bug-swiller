package com.capgemini.osce.quaff.bugswiller

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class BugSwillerApplication

fun main(args: Array<String>) {
    runApplication<BugSwillerApplication>(*args)
}
